# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# 0.1.0 (2019-03-03)


### Features

* can filter by stack name and resource type ([3ebbc9f](https://gitlab.com/kabo/list-cloudformation-resources/commit/3ebbc9f))
* can output in text or json, resources are sorted ([c3dc8fa](https://gitlab.com/kabo/list-cloudformation-resources/commit/c3dc8fa))
