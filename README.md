# list-cloudformation-resources

Does what it says, lists AWS CloudFormation resources.

## Installation

```
yarn global add list-cloudformation-resources
```

or

```
npm i -g list-cloudformation-resources
```

## Example usage

Help

```
list-cloudformation-resources -h
```

Which S3 buckets don't belong to a CloudFormation stack?

```
list-cloudformation-resources -f text -t AWS::S3::Bucket > cf-buckets.txt
aws s3 ls > all-buckets.txt
comm -3 all-buckets.txt cf-buckets.txt
```

## License

list-cloudformation-resources is licensed under the terms of the MIT license.

