#!/usr/bin/env node

const AWS = require('aws-sdk'),
  nanomatch = require('nanomatch'),
  partial = require('mcpartial'),
  R = require('ramda'),
  argv = require('yargs')
    .options({
      s: {
        alias: 'stack-name',
        type: 'string',
        describe: 'Only consider stacks with this name. Supports nanomatch syntax, see https://github.com/micromatch/nanomatch#features',
      },
      t: {
        alias: 'resource-type',
        type: 'string',
        describe: 'Only consider resources with this resource type, see https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html',
      },
      f: {
        alias: 'format',
        type: 'string',
        default: 'json',
        describe: 'Output format',
        choices: ['json', 'text'],
      },
    })
    .help('h')
    .alias('h', 'help')
    .argv

const listStacks = async ({ cfn, nextToken }) => {
    const params = { NextToken: nextToken },
      { NextToken, StackSummaries } = await cfn.listStacks(params).promise()
    return NextToken ? [ ...StackSummaries, ...await listStacks({ cfn, nextToken: NextToken }) ] : StackSummaries
  },
  listResources = async ({ cfn, StackName, nextToken }) => {
    const params = { StackName, NextToken: nextToken },
      { NextToken, StackResourceSummaries } = await cfn.listStackResources(params).promise()
    return NextToken ? [ ...StackResourceSummaries, ...await listResources({ cfn, StackName, nextToken: NextToken }) ] : StackResourceSummaries
  },
  getStackNames = R.map(R.prop('StackName')),
  getResourceIds = R.map(R.prop('PhysicalResourceId')),
  sortStrings = R.sort((a, b) => a.localeCompare(b)),
  formatText = R.join('\n'),
  formatJson = R.curry(JSON.stringify)(R.__, false, 2);

/* eslint-disable fp/no-nil, fp/no-unused-expression, no-console */
(async () => {
  const cfn = new AWS.CloudFormation(),
    lr = partial(listResources, { cfn }),
    nm = R.curry(nanomatch),
    stackNames = await listStacks({ cfn })
      .then(getStackNames)
      .then(R.when(
        () => !!argv.s,
        nm(R.__, argv.s, {})
      )),
    resources = await Promise.all(R.map((StackName) => lr({ StackName }), stackNames))
      .then(R.flatten)
      .then(R.when(
        () => !!argv.t,
        R.filter(R.propEq('ResourceType', argv.t))
      ))
      .then(getResourceIds)
      .then(sortStrings)
  return argv.f === 'text' ? console.log(formatText(resources))
    : console.log(formatJson(resources))
})()
/* eslint-enable */
